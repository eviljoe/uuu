import argparse
import os.path
from argparse import ArgumentParser

from expects import be, equal, expect, match
from jnscommons import jnsos
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

import uuu
from updaters import atom, cabal, choco, cygwin, git, gitd, initjns, npm, pip, pip3, rustgit, svn, svnd
from updaters.abstractupdater import AbstractUpdater
from utils import uuuconfig
from utils.uuurunner import UUURunner

with description(uuu) as self:
    with after.each:
        unstub()

    with description(uuu.main):
        with before.each:
            self.opts = mock({'no_config': None})
            when(uuu)._parse_args(...).thenReturn(self.opts)
            when(uuu)._read_config_file(...)
            when(uuu)._validate_opts(...)
            when(uuu)._sudo(...)
            when(uuu)._update(...)

        with it('gets the options'):
            uuu.main()
            verify(uuu)._parse_args()

        with it('reads the config file when the options allow for it'):
            self.opts.no_config = True
            uuu.main()
            verify(uuu, times(0))._read_config_file(...)

        with it('does not read the config file when the options prevent it'):
            self.opts.no_config = False
            uuu.main()
            verify(uuu)._read_config_file(...)

        with it('validates the options'):
            uuu.main()
            verify(uuu)._validate_opts(...)

        with it('acquires super user permissions'):
            uuu.main()
            verify(uuu)._sudo(...)

        with it('updates... all the things!'):
            uuu.main()
            verify(uuu)._update(...)

    with description(uuu._parse_args):
        with before.each:
            self.parser = mock(ArgumentParser)
            self.args = mock({'foo': 'bar'})
            self.opts = mock({'foo': 'baz'})
            self.updater1 = mock(AbstractUpdater)
            self.updater2 = mock(AbstractUpdater)

            when(self.parser).add_argument(...)
            when(self.parser).parse_args(...).thenReturn(self.args)
            when(argparse).ArgumentParser(...).thenReturn(self.parser)
            when(self.updater1).add_help_argument(...)
            when(self.updater2).add_help_argument(...)

            when(uuu)._create_help_epilog(...).thenReturn('foo')
            when(uuu)._to_options(self.args).thenReturn(self.opts)
            when(uuu)._updaters(...).thenReturn([self.updater1, self.updater2])

        with it('returns the parsed arguments'):
            expect(uuu._parse_args()).to(be(self.opts))

        with it('adds a help argument for each updater'):
            uuu._parse_args()
            verify(self.updater1).add_help_argument(self.parser)
            verify(self.updater2).add_help_argument(self.parser)

    with description(uuu._create_help_epilog):
        with before.each:
            when(uuu)._get_default_config_file_name(...).thenReturn('~DEFAULT_CONFIG_FILE_NAME~')

        with it('returns the epilog'):
            expect(uuu._create_help_epilog()).to(match('.{1,}'))

        with it('mentions the default config file name'):
            expect(uuu._create_help_epilog()).to(match('.*~DEFAULT_CONFIG_FILE_NAME~.*'))

    with description(uuu._to_options):
        with before.each:
            self.ns = mock({})

        with it('converts the atom flag'):
            self.ns.atom = True
            expect(uuu._to_options(self.ns).atom).to(equal(True))

        with it('converts the cabal packages'):
            self.ns.cabal_packages = ['foo', 'bar']
            expect(uuu._to_options(self.ns).cabal_packages).to(equal(['foo', 'bar']))

        with it('converts the choco flag'):
            self.ns.choco = True
            expect(uuu._to_options(self.ns).choco).to(equal(True))

        with it('converts the cygwin exe'):
            self.ns.cygwin_exe = 'foo'
            expect(uuu._to_options(self.ns).cygwin_exe).to(equal('foo'))

        with it('converts the dry run flag'):
            self.ns.dry_run = True
            expect(uuu._to_options(self.ns).dry_run).to(equal(True))

        with it('converts the git dirs'):
            self.ns.git_dirs = ['foo', 'bar']
            expect(uuu._to_options(self.ns).git_dirs).to(equal(['foo', 'bar']))

        with it('converts the gitd dirs'):
            self.ns.gitd_dirs = ['foo', 'bar']
            expect(uuu._to_options(self.ns).gitd_dirs).to(equal(['foo', 'bar']))

        with it('converts the init jns flag'):
            self.ns.init_jns = True
            expect(uuu._to_options(self.ns).init_jns).to(equal(True))

        with it('converts the no config flag'):
            self.ns.no_config = True
            expect(uuu._to_options(self.ns).no_config).to(equal(True))

        with it('converts the npm packages'):
            self.ns.npm_packages = ['foo', 'bar']
            expect(uuu._to_options(self.ns).npm_packages).to(equal(['foo', 'bar']))

        with it('converts the pip3 packages'):
            self.ns.pip3_packages = ['foo', 'bar']
            expect(uuu._to_options(self.ns).pip3_packages).to(equal(['foo', 'bar']))

        with it('converts the pip packages'):
            self.ns.pip_packages = ['foo', 'bar']
            expect(uuu._to_options(self.ns).pip_packages).to(equal(['foo', 'bar']))

        with it('converts the rust git projects'):
            self.ns.rust_git_projects = ['foo', 'bar']
            expect(uuu._to_options(self.ns).rust_git_projects).to(equal(['foo', 'bar']))

        with it('converts the svn dirs'):
            self.ns.svn_dirs = ['foo', 'bar']
            expect(uuu._to_options(self.ns).svn_dirs).to(equal(['foo', 'bar']))

        with it('converts the svnd dirs'):
            self.ns.svnd_dirs = ['foo', 'bar']
            expect(uuu._to_options(self.ns).svnd_dirs).to(equal(['foo', 'bar']))

        with it('converts the verbose flag'):
            self.ns.verbose = True
            expect(uuu._to_options(self.ns).verbose).to(equal(True))

    with description(uuu._read_config_file):
        with before.each:
            when(uuuconfig).read_config_file(...)
            when(uuu)._get_default_config_file_name(...).thenReturn('/config/file/name')
            when(uuu)._updaters(...).thenReturn([])

        with it('reads the config file'):
            opts = mock({'foo': 'bar'})
            uuu._read_config_file(opts)
            verify(uuuconfig).read_config_file(updaters=[], opts=opts, config_file_name='/config/file/name')

    with description(uuu._get_default_config_file_name):
        with before.each:
            when(os.path).expanduser(...).thenReturn('/home/mock')

        with it('returns $HOME/.jns/uuu'):
            expect(uuu._get_default_config_file_name()).to(equal('/home/mock/.jns/uuu'))

    with description(uuu._validate_opts):
        with before.each:
            self.updater1 = mock(AbstractUpdater)
            self.updater2 = mock(AbstractUpdater)

            when(self.updater1).validate_opts(...)
            when(self.updater2).validate_opts(...)

            when(uuu)._updaters(...).thenReturn([self.updater1, self.updater2])

        with it('validates the given opts with each updater'):
            opts = mock({'foo': 'bar'})
            uuu._validate_opts(opts)

            verify(self.updater1).validate_opts(opts)
            verify(self.updater2).validate_opts(opts)

    with description(uuu._sudo):
        with before.each:
            self.opts = mock({})
            self.runner = mock(UUURunner)

            when(uuu)._os_has_sudo(...).thenReturn(True)
            when(uuu)._root_required(...).thenReturn(True)

            when(self.runner).run(...)

        with it('requests sudo when the OS supports sudo and root is required'):
            uuu._sudo(self.opts, self.runner)
            verify(self.runner).run(...)

        with it('does not request sudo when the OS does not support sudo'):
            when(uuu)._os_has_sudo(...).thenReturn(False)
            uuu._sudo(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)

        with it('does not request sudo when root is not required'):
            when(uuu)._root_required(...).thenReturn(False)
            uuu._sudo(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)

    with description(uuu._os_has_sudo):
        with before.each:
            when(jnsos).is_windows(...).thenReturn(False)
            when(jnsos).is_cygwin(...).thenReturn(False)

        with it('returns true when not in Windows or Cygwin'):
            expect(uuu._os_has_sudo()).to(equal(True))

        with it('returns false when in Windows'):
            when(jnsos).is_windows(...).thenReturn(True)
            expect(uuu._os_has_sudo()).to(equal(False))

        with it('returns false when in Cygwin'):
            when(jnsos).is_cygwin(...).thenReturn(True)
            expect(uuu._os_has_sudo()).to(equal(False))

    with description(uuu._root_required):
        with before.each:
            self.opts = mock({})
            self.updater1 = mock(AbstractUpdater)
            self.updater2 = mock(AbstractUpdater)
            self.updater3 = mock(AbstractUpdater)

            when(self.updater1).is_root_required(...).thenReturn(False)
            when(self.updater2).is_root_required(...).thenReturn(False)
            when(self.updater3).is_root_required(...).thenReturn(False)

            when(uuu)._updaters(...).thenReturn([self.updater1, self.updater2, self.updater3])

        with it('returns false when no updater requires root'):
            expect(uuu._root_required(self.opts)).to(equal(False))

        with it('returns true when an updater requires root'):
            when(self.updater2).is_root_required(...).thenReturn(True)
            expect(uuu._root_required(self.opts)).to(equal(True))

    with description(uuu._update):
        with before.each:
            self.opts = mock({})
            self.runner = mock(UUURunner)
            self.updater1 = mock(AbstractUpdater)
            self.updater2 = mock(AbstractUpdater)

            when(self.updater1).update(...)
            when(self.updater2).update(...)

            when(uuu)._updaters(...).thenReturn([self.updater1, self.updater2])

        with it('updates using each of the updaters'):
            uuu._update(self.opts, self.runner)
            verify(self.updater2).update(opts=self.opts, runner=self.runner)
            verify(self.updater1).update(opts=self.opts, runner=self.runner)

    with description(uuu._updaters):
        with before.each:
            self.atomUpdater = mock({'atom': True})
            self.cabalUpdater = mock({'cabal': True})
            self.chocoUpdater = mock({'choco': True})
            self.cygwinUpdater = mock({'cygwin': True})
            self.gitUpdater = mock({'git': True})
            self.gitdUpdater = mock({'gitd': True})
            self.initjnsUpdater = mock({'initjns': True})
            self.npmUpdater = mock({'npm': True})
            self.pipUpdater = mock({'pip': True})
            self.pip3Updater = mock({'pip3': True})
            self.rustgitUpdater = mock({'rustgit': True})
            self.svnUpdater = mock({'svn': True})
            self.svndUpdater = mock({'svnd': True})

            when(atom).AtomUpdater(...).thenReturn(self.atomUpdater)
            when(cabal).CabalUpdater(...).thenReturn(self.cabalUpdater)
            when(choco).ChocoUpdater(...).thenReturn(self.chocoUpdater)
            when(cygwin).CygwinUpdater(...).thenReturn(self.cygwinUpdater)
            when(git).GitUpdater(...).thenReturn(self.gitUpdater)
            when(gitd).GitdUpdater(...).thenReturn(self.gitdUpdater)
            when(initjns).InitJNSUpdater(...).thenReturn(self.initjnsUpdater)
            when(npm).NPMUpdater(...).thenReturn(self.npmUpdater)
            when(pip).PipUpdater(...).thenReturn(self.pipUpdater)
            when(pip3).Pip3Updater(...).thenReturn(self.pip3Updater)
            when(rustgit).RustGitUpdater(...).thenReturn(self.rustgitUpdater)
            when(svn).SVNUpdater(...).thenReturn(self.svnUpdater)
            when(svnd).SVNDUpdater(...).thenReturn(self.svndUpdater)

        with it('returns each of the updaters'):
            expect(uuu._updaters()).to(equal([
                self.atomUpdater,
                self.cabalUpdater,
                self.chocoUpdater,
                self.cygwinUpdater,
                self.gitUpdater,
                self.gitdUpdater,
                self.initjnsUpdater,
                self.npmUpdater,
                self.pipUpdater,
                self.pip3Updater,
                self.rustgitUpdater,
                self.svnUpdater,
                self.svndUpdater,
            ]))
