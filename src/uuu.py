#!/usr/bin/env python3


import argparse
import os.path
import sys
from argparse import Namespace
from argparse import RawDescriptionHelpFormatter

from jnscommons import jnsos
from jnscommons import jnsstr

from updaters import atom
from updaters import cabal
from updaters import choco
from updaters import cygwin
from updaters import git
from updaters import gitd
from updaters import initjns
from updaters import npm
from updaters import pip
from updaters import pip3
from updaters import rustgit
from updaters import svn
from updaters import svnd
from updaters.abstractupdater import AbstractUpdater
from utils import uuuconfig
from utils import uuurunner
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

VERSION = '3.0.0'


def main() -> None:
    opts = _parse_args()
    runner = uuurunner.UUURunner()

    if not opts.no_config:
        _read_config_file(opts)

    _validate_opts(opts)
    _sudo(opts, runner)
    _update(opts, runner)


def _parse_args() -> UUUOptions:
    parser = argparse.ArgumentParser(description='Update... all the things!', epilog=_create_help_epilog(),
                                     formatter_class=RawDescriptionHelpFormatter)

    for updater in _updaters():
        updater.add_help_argument(parser)

    parser.add_argument('--dry-run', action='store_true', default=False, dest='dry_run',
                        help='Output what actions will be performed without taking them (default: %(default)s)')
    parser.add_argument('--no-config', action='store_true', default=False, dest='no_config',
                        help='Do not read the configuration file (default: %(default)s)')
    parser.add_argument('--verbose', action='store_true', default=False, dest='verbose',
                        help='Adds more output (default: %(default)s)')

    return _to_options(parser.parse_args())


def _create_help_epilog() -> str:
    e = []
    e.append('CONFIGURATION FILE')
    e.append('In addition to the command line arguments, a configuration file can be used to  specify the updates that '
             'should be made.  `{}\' will look for that file here:'.format(os.path.basename(sys.argv[0])))
    e.append('  {}'.format(_get_default_config_file_name()))
    e.append('Each line of the file should be in the following format:')
    e.append('  <COMMAND> [ARGUMENT]')
    e.append('The commands are the same as their corresponding command line argument, but without the leading --.  For '
             'example, to specify a git repository:')
    e.append('  git ~/Documents/git/junk-n-stuff')
    e.append('For commands without an argument, just specify the command.  For example:')
    e.append('  atom')
    e.append('The rustgit updater requires a more complex argument format that contains three parts separated by a '
             'comma:')
    e.append('  rustgit GIT_URL,PROJECT_HOME,EXE_NAME')
    e.append('The GIT_URL is the URL that you would use to clone a Git project.  The PROJECT_HOME is the directory that'
             'should be the root for the project.  The EXE_NAME is the command that should be used to invoke the '
             'program after it has been installed & updated.  For example:')
    e.append('  rustgit https://gitlab.com/eviljoe/sysmonitor.git,/opt/local/sysmonitor,sysmonitor')
    e.append('')
    e.append('CONFIGURATION FILE NOTES')
    e.append(' * Empty lines and lines containing only whitespace are ignored')
    e.append(' * Lines whose command starts with # are ignored')
    e.append(' * Whitespace before and after the command is ignored')
    e.append(' * Arguments can contain whitespace but cannot start with a whitespace character')
    e.append(' * Whitespace at the end of an argument will be considered to be part of that argument')

    return jnsstr.wrap_str_array(e)


def _to_options(nspace: Namespace) -> UUUOptions:
    return UUUOptions(
        atom=nspace.atom,
        cabal_packages=nspace.cabal_packages,
        choco=nspace.choco,
        cygwin_exe=nspace.cygwin_exe,
        dry_run=nspace.dry_run,
        git_dirs=nspace.git_dirs,
        gitd_dirs=nspace.gitd_dirs,
        init_jns=nspace.init_jns,
        no_config=nspace.no_config,
        npm_packages=nspace.npm_packages,
        pip3_packages=nspace.pip3_packages,
        pip_packages=nspace.pip_packages,
        rust_git_projects=nspace.rust_git_projects,
        svn_dirs=nspace.svn_dirs,
        svnd_dirs=nspace.svnd_dirs,
        verbose=nspace.verbose,
    )


def _read_config_file(opts: UUUOptions) -> None:
    uuuconfig.read_config_file(updaters=_updaters(), opts=opts, config_file_name=_get_default_config_file_name())


def _get_default_config_file_name() -> str:
    return os.path.join(os.path.expanduser('~'), '.jns', 'uuu')


def _validate_opts(opts: UUUOptions) -> None:
    for updater in _updaters():
        updater.validate_opts(opts)


def _sudo(opts: UUUOptions, runner: UUURunner) -> None:
    if _os_has_sudo() and _root_required(opts):
        runner.run(opts=opts, cmds=[['sudo', 'echo', 'privileges', 'escalated']])


def _os_has_sudo() -> bool:
    return not (jnsos.is_windows() or jnsos.is_cygwin())


def _root_required(opts: UUUOptions) -> bool:
    return next((u for u in _updaters() if u.is_root_required(opts)), None) is not None


def _update(opts: UUUOptions, runner: UUURunner) -> None:
    for updater in _updaters():
        updater.update(opts=opts, runner=runner)


def _updaters() -> [AbstractUpdater]:
    return [
        atom.AtomUpdater(),
        cabal.CabalUpdater(),
        choco.ChocoUpdater(),
        cygwin.CygwinUpdater(),
        git.GitUpdater(),
        gitd.GitdUpdater(),
        initjns.InitJNSUpdater(),
        npm.NPMUpdater(),
        pip.PipUpdater(),
        pip3.Pip3Updater(),
        rustgit.RustGitUpdater(),
        svn.SVNUpdater(),
        svnd.SVNDUpdater(),
    ]


if __name__ == '__main__':
    main()
