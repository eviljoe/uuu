from argparse import ArgumentParser

from updaters.abstractupdater import AbstractUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

_CMD = 'init-jns'


class InitJNSUpdater(AbstractUpdater):
    def add_help_argument(self, parser: ArgumentParser) -> None:
        parser.add_argument('--' + _CMD, action='store_true', default=False, dest='init_jns',
                            help="Invoke `init-jns' from junk-n-stuff (default: %(default)s)")

    def get_config_command(self) -> str:
        return _CMD

    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        opts.init_jns = True

    def validate_opts(self, opts: UUUOptions) -> None:
        return

    def is_root_required(self, opts: UUUOptions) -> bool:
        return False

    def update(self, opts: UUUOptions, runner: UUURunner) -> None:
        if opts.init_jns:
            runner.run(opts=opts, cmds=[['init-jns']], title='initializing junk-n-stuff')
