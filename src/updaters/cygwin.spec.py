from argparse import ArgumentParser

from expects import be_false, equal, expect, raise_error
from jnscommons import jnsos
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

from updaters import cygwin
from updaters.cygwin import CygwinUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

with description(cygwin.CygwinUpdater) as self:
    with before.each:
        self.updater = CygwinUpdater()
        self.opts = mock({'cygwin_exe': None, 'dry_run': None}, spec=UUUOptions)

    with after.each:
        unstub()

    with description(CygwinUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(CygwinUpdater.get_config_command):
        with it('returns "cygwin"'):
            expect(self.updater.get_config_command()).to(equal('cygwin'))

    with description(CygwinUpdater.update_opts_for_command):
        with before.each:
            self.ccmd = mock(ConfigCommand)
            when(self.updater).update_opts_set_file(...)

        with it('updates the options to set a file'):
            self.updater.update_opts_for_command(self.opts, self.ccmd)
            verify(self.updater).update_opts_set_file(self.ccmd, self.opts, 'cygwin_exe')

    with description(CygwinUpdater.validate_opts):
        with before.each:
            self.opts.cygwin_exe = '/bin/cygwin'
            self.opts.dry_run = False
            when(jnsos).is_windows(...).thenReturn(False)
            when(jnsos).is_cygwin(...).thenReturn(False)

        with it('raises an error when in an unsupported OS'):
            expect(lambda: self.updater.validate_opts(self.opts)).to(raise_error)

        with it('does not raise an error when the cygwin exe path is empty'):
            self.opts.cygwin_exe = ''
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('does not raise an error when the cygwin exe path is None'):
            self.opts.cygwin_exe = None
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('does not raise an error when performing a dry run'):
            self.opts.dry_run = True
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('does not raise an error when in Windows'):
            when(jnsos).is_windows(...).thenReturn(True)
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('does not raise an error when in Cygwin'):
            when(jnsos).is_cygwin(...).thenReturn(True)
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

    with description(CygwinUpdater.is_root_required):
        with it('returns false'):
            expect(self.updater.is_root_required(self.opts)).to(be_false)

    with description(CygwinUpdater.update):
        with before.each:
            self.runner = mock(UUURunner)
            when(self.runner).run(...)

        with it('updates cygwin when the cygwin exe is not empty'):
            self.opts.cygwin_exe = '/bin/cygwin'
            self.updater.update(self.opts, self.runner)
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['/bin/cygwin', '--quiet-mode', '--no-desktop']],
                title='updating cygwin packages',
            )

        with it('does not update cygwin when the cygwin exe is empty'):
            self.opts.cygwin_exe = ''
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)

        with it('does not update cygwin when the cygwin exe is None'):
            self.opts.cygwin_exe = None
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)
