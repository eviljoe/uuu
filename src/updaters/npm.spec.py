from argparse import ArgumentParser

from expects import be_false, be_true, equal, expect, raise_error
from jnscommons import jnsos
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

from updaters import npm
from updaters.npm import NPMUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

with description(npm.NPMUpdater) as self:
    with before.each:
        self.updater = NPMUpdater()
        self.opts = mock({'npm_packages': None}, spec=UUUOptions)

    with after.each:
        unstub()

    with description(NPMUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(NPMUpdater.get_config_command):
        with it('returns "npm"'):
            expect(self.updater.get_config_command()).to(equal('npm'))

    with description(NPMUpdater.update_opts_for_command):
        with before.each:
            self.ccmd = mock(ConfigCommand)
            when(self.updater).update_opts_add_argument(...)

        with it('adds an argument to the options'):
            self.opts.npm_packages = ['a', 'b']
            self.updater.update_opts_for_command(self.opts, self.ccmd)
            verify(self.updater).update_opts_add_argument(ccmd=self.ccmd, arguments=['a', 'b'])

    with description(NPMUpdater.validate_opts):
        with it('does not raise an error'):
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

    with description(NPMUpdater.is_root_required):
        with it('returns true when there are NPM packages'):
            self.opts.npm_packages = ['a', 'b']
            expect(self.updater.is_root_required(self.opts)).to(be_true)

        with it('returns false when the NPM packages are empty'):
            self.opts.npm_packages = []
            expect(self.updater.is_root_required(self.opts)).to(be_false)

        with it('returns false when the NPM packages are None'):
            self.opts.npm_packages = None
            expect(self.updater.is_root_required(self.opts)).to(be_false)

    with description(NPMUpdater.update):
        with before.each:
            self.runner = mock(UUURunner)
            when(self.runner).run(...)
            when(jnsos).is_linux(...).thenReturn(False)

        with it('updates each npm package with sudo when in Linux'):
            self.opts.npm_packages = ['a', 'b']
            when(jnsos).is_linux(...).thenReturn(True)
            self.updater.update(self.opts, self.runner)

            verify(self.runner).run(
                opts=self.opts,
                cmds=[['sudo', 'npm', 'update', '-g', 'a', 'b']],
                title='updating npm packages: a, b'
            )

        with it('updates each npm package without sudo when not in Linux'):
            self.opts.npm_packages = ['a', 'b']
            when(jnsos).is_linux(...).thenReturn(False)
            self.updater.update(self.opts, self.runner)

            verify(self.runner).run(
                opts=self.opts,
                cmds=[['npm', 'update', '-g', 'a', 'b']],
                title='updating npm packages: a, b'
            )

        with it('does not update anything when the npm packages are empty'):
            self.opts.npm_packages = []
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)
