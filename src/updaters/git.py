from argparse import ArgumentParser

from jnscommons import jnsvalid

from updaters.abstractupdater import AbstractRepoUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions

_CMD = 'git'


class GitUpdater(AbstractRepoUpdater):
    def add_help_argument(self, parser: ArgumentParser) -> None:
        parser.add_argument('--' + _CMD, action='append', default=[], metavar='git_repo', dest='git_dirs',
                            help='Specify a git repository to be updated')

    def get_config_command(self) -> str:
        return _CMD

    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        self.update_opts_add_directory(ccmd=ccmd, directories=self.get_repos(opts))

    def validate_opts(self, opts: UUUOptions) -> None:
        jnsvalid.validate_is_directories(opts.git_dirs)

    def get_config_directory(self) -> str:
        return '.' + _CMD

    def get_repo_type(self) -> str:
        return _CMD

    def get_repos(self, opts: UUUOptions) -> [str]:
        return opts.git_dirs

    def get_update_cmd(self) -> [str]:
        return ['git', 'pull']
