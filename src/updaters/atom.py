from argparse import ArgumentParser

from jnscommons import jnsos

from updaters.abstractupdater import AbstractUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

_CMD = 'atom'


class AtomUpdater(AbstractUpdater):
    def add_help_argument(self, parser: ArgumentParser) -> None:
        parser.add_argument('--' + _CMD, action='store_true', default=False, dest='atom',
                            help="Specify that Atom's packages should be updated (default: %(default)s)")

    def get_config_command(self) -> str:
        return _CMD

    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        opts.atom = True

    def validate_opts(self, opts: UUUOptions) -> None:
        return

    def is_root_required(self, opts: UUUOptions) -> bool:
        return False

    def update(self, opts: UUUOptions, runner: UUURunner) -> None:
        if opts.atom:
            cmd = []

            if jnsos.is_cygwin():
                cmd.extend(['cmd', '/C', 'apm'])
            elif jnsos.is_windows():
                cmd.append('apm.cmd')
            elif jnsos.is_linux():
                cmd.append('apm')

            cmd.extend(['update', '--no-confirm'])

            runner.run(opts=opts, cmds=[cmd], title="updating atom's packages")
