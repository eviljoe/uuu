from argparse import ArgumentParser

from expects import be_none, equal, expect
from jnscommons import jnsvalid
from mamba import after, before, description, it
from mockito import mock, unstub, verify, when

from updaters import gitd
from updaters.gitd import GitdUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions

with description(gitd.GitdUpdater) as self:
    with before.each:
        self.updater = GitdUpdater()

    with after.each:
        unstub()

    with description(GitdUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(GitdUpdater.get_config_command):
        with it('returns "gitd"'):
            expect(self.updater.get_config_command()).to(equal("gitd"))

    with description(GitdUpdater.update_opts_for_command):
        with before.each:
            self.opts = mock(UUUOptions)
            self.ccmd = mock(ConfigCommand)

            when(self.updater).update_opts_add_directory(...)
            when(self.updater).get_repo_dirs(...).thenReturn(['a', 'b'])

        with it('adds a directory to the options'):
            self.updater.update_opts_for_command(self.opts, self.ccmd)
            verify(self.updater).update_opts_add_directory(ccmd=self.ccmd, directories=['a', 'b'])

    with description(GitdUpdater.validate_opts):
        with before.each:
            self.opts = mock({'gitd_dirs': ['a', 'b']}, spec=UUUOptions)
            when(jnsvalid).validate_is_directories(...)

        with it('validates that each git directory is a valid directory'):
            self.updater.validate_opts(self.opts)
            verify(jnsvalid).validate_is_directories(['a', 'b'])

    with description(GitdUpdater.get_config_directory):
        with it('returns ".git"'):
            expect(self.updater.get_config_directory()).to(equal(".git"))

    with description(GitdUpdater.get_repo_dirs):
        with before.each:
            self.opts = mock({'gitd_dirs': ['a', 'b']}, spec=UUUOptions)

        with it('returns the gitd directories'):
            expect(self.updater.get_repo_dirs(self.opts)).to(equal(['a', 'b']))

    with description(GitdUpdater.get_repos):
        with before.each:
            self.opts = mock(UUUOptions)

        with it('returns None'):
            expect(self.updater.get_repos(self.opts)).to(be_none)

    with description(GitdUpdater.get_repo_type):
        with it('returns "git"'):
            expect(self.updater.get_repo_type()).to(equal("git"))

    with description(GitdUpdater.get_update_cmd):
        with it('returns "git pull"'):
            expect(self.updater.get_update_cmd()).to(equal(['git', 'pull']))
