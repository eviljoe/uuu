from argparse import ArgumentParser

from expects import be_none, equal, expect
from jnscommons import jnsvalid
from mamba import after, before, description, it
from mockito import mock, unstub, verify, when

from updaters import svnd
from updaters.svnd import SVNDUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions

with description(svnd.SVNDUpdater) as self:
    with before.each:
        self.updater = SVNDUpdater()

    with after.each:
        unstub()

    with description(SVNDUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(SVNDUpdater.get_config_command):
        with it('returns "svnd"'):
            expect(self.updater.get_config_command()).to(equal("svnd"))

    with description(SVNDUpdater.update_opts_for_command):
        with before.each:
            self.opts = mock(UUUOptions)
            self.ccmd = mock(ConfigCommand)

            when(self.updater).update_opts_add_directory(...)
            when(self.updater).get_repo_dirs(...).thenReturn(['a', 'b'])

        with it('adds a directory to the options'):
            self.updater.update_opts_for_command(self.opts, self.ccmd)
            verify(self.updater).update_opts_add_directory(ccmd=self.ccmd, directories=['a', 'b'])

    with description(SVNDUpdater.validate_opts):
        with before.each:
            self.opts = mock({'svnd_dirs': ['a', 'b']}, spec=UUUOptions)
            when(jnsvalid).validate_is_directories(...)

        with it('validates that each git directory is a valid directory'):
            self.updater.validate_opts(self.opts)
            verify(jnsvalid).validate_is_directories(['a', 'b'])

    with description(SVNDUpdater.get_config_directory):
        with it('returns ".svn"'):
            expect(self.updater.get_config_directory()).to(equal(".svn"))

    with description(SVNDUpdater.get_repo_dirs):
        with before.each:
            self.opts = mock({'svnd_dirs': ['a', 'b']}, spec=UUUOptions)

        with it('returns the svnd directories'):
            expect(self.updater.get_repo_dirs(self.opts)).to(equal(['a', 'b']))

    with description(SVNDUpdater.get_repos):
        with before.each:
            self.opts = mock(UUUOptions)

        with it('returns None'):
            expect(self.updater.get_repos(self.opts)).to(be_none)

    with description(SVNDUpdater.get_repo_type):
        with it('returns "svn"'):
            expect(self.updater.get_repo_type()).to(equal("svn"))

    with description(SVNDUpdater.get_update_cmd):
        with it('returns "svn update --non-interactive"'):
            expect(self.updater.get_update_cmd()).to(equal(['svn', 'update', '--non-interactive']))
