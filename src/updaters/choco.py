from argparse import ArgumentParser

from jnscommons import jnsos

from updaters.abstractupdater import AbstractUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

_CMD = 'choco'


class ChocoUpdater(AbstractUpdater):
    def add_help_argument(self, parser: ArgumentParser) -> None:
        parser.add_argument('--' + _CMD, action='store_true', default=False, dest='choco',
                            help='Specify that all Chocolatey packages should be updated (default: %(default)s)')

    def get_config_command(self) -> str:
        return _CMD

    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        opts.choco = True

    def validate_opts(self, opts: UUUOptions) -> None:
        if opts.choco and not (opts.dry_run or jnsos.is_windows() or jnsos.is_cygwin()):
            raise NotChocoOSError(
                'Can only update Chocolatey packages when in Windows, in Cygwin, or performing a dry run.')

    def is_root_required(self, opts: UUUOptions) -> bool:
        return False

    def update(self, opts: UUUOptions, runner: UUURunner) -> None:
        if opts.choco:
            cmds = [[
                'Powershell',
                '-Command',
                '& { Start-Process "choco" -ArgumentList @("upgrade", "all") -Verb RunAs }'
            ]]

            runner.run(opts=opts, cmds=cmds, title='updating chocolatey packages')


class NotChocoOSError(Exception):
    pass
