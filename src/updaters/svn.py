from argparse import ArgumentParser

from jnscommons import jnsvalid

from updaters.abstractupdater import AbstractRepoUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions

_CMD = 'svn'


class SVNUpdater(AbstractRepoUpdater):
    def add_help_argument(self, parser: ArgumentParser) -> None:
        parser.add_argument('--' + _CMD, action='append', default=[], metavar='svn_repo', dest='svn_dirs',
                            help='Specify an SVN repository to be updated')

    def get_config_command(self) -> str:
        return _CMD

    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        self.update_opts_add_directory(ccmd=ccmd, directories=self.get_repos(opts))

    def validate_opts(self, opts: UUUOptions) -> None:
        jnsvalid.validate_is_directories(opts.svn_dirs)

    def get_config_directory(self) -> str:
        return '.' + _CMD

    def get_repo_type(self) -> str:
        return _CMD

    def get_repos(self, opts: UUUOptions) -> [str]:
        return opts.svn_dirs

    def get_update_cmd(self) -> [str]:
        return ['svn', 'update', '--non-interactive']
