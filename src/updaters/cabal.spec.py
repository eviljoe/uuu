from argparse import ArgumentParser

from expects import be_false, equal, expect, raise_error
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

from updaters import cabal
from updaters.cabal import CabalUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

with description(cabal.CabalUpdater) as self:
    with before.each:
        self.updater = CabalUpdater()
        self.opts = mock({'cabal_packages': None}, spec=UUUOptions)

    with after.each:
        unstub()

    with description(CabalUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(CabalUpdater.get_config_command):
        with it('returns "cabal"'):
            expect(self.updater.get_config_command()).to(equal('cabal'))

    with description(CabalUpdater.update_opts_for_command):
        with before.each:
            self.ccmd = mock(ConfigCommand)
            when(self.updater).update_opts_add_argument(...)

        with it('adds an argument to the options'):
            self.opts.cabal_packages = ['a', 'b']
            self.updater.update_opts_for_command(self.opts, self.ccmd)
            verify(self.updater).update_opts_add_argument(ccmd=self.ccmd, arguments=['a', 'b'])

    with description(CabalUpdater.validate_opts):
        with it('does not raise an error'):
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

    with description(CabalUpdater.is_root_required):
        with it('returns false'):
            expect(self.updater.is_root_required(self.opts)).to(be_false)

    with description(CabalUpdater.update):
        with before.each:
            self.runner = mock(UUURunner)
            when(self.runner).run(...)

        with it('install each cabal package and refreshes before installing the first package'):
            self.opts.cabal_packages = ['foo', 'bar', 'baz']
            self.updater.update(self.opts, self.runner)

            verify(self.runner).run(
                opts=self.opts,
                cmds=[['cabal', 'update'], ['cabal', 'install', 'foo']],
                title='updating cabal package: foo',
            )
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['cabal', 'install', 'bar']],
                title='updating cabal package: bar',
            )
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['cabal', 'install', 'baz']],
                title='updating cabal package: baz',
            )

        with it('does not update anything when the cabal packages are empty'):
            self.opts.cabal_packages = []
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)
