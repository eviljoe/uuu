import os.path
from argparse import ArgumentParser

from expects import be_false, be_true, equal, expect, raise_error
from jnscommons import jnsos, jnsvalid
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

from updaters import rustgit
from updaters.rustgit import RustGitProjectMetaData, RustGitUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

with description(rustgit.RustGitUpdater) as self:
    with before.each:
        self.updater = RustGitUpdater()

    with after.each:
        unstub()

    with description(RustGitUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(RustGitUpdater.get_config_command):
        with it('returns "rustgit"'):
            expect(self.updater.get_config_command()).to(equal('rustgit'))

    with description(RustGitUpdater.update_opts_for_command):
        with before.each:
            self.opts = mock(UUUOptions)
            self.ccmd = mock(ConfigCommand)
            when(self.updater).update_opts_add_argument(...)

        with it('adds an argument to the options'):
            self.opts.rust_git_projects = ['a', 'b']
            self.updater.update_opts_for_command(self.opts, self.ccmd)
            verify(self.updater).update_opts_add_argument(ccmd=self.ccmd, arguments=['a', 'b'])

    with description(RustGitUpdater.validate_opts):
        with before.each:
            self.opts = mock({'rust_git_projects': [{}, {}]}, spec=UUUOptions)
            self.md1 = mock({'project_home': '/dir1'}, spec=RustGitProjectMetaData)
            self.md2 = mock({'project_home': '/dir2'}, spec=RustGitProjectMetaData)

            when(self.updater)._get_all_project_meta_data(...).thenReturn([self.md1, self.md2])
            when(os.path).exists('/dir1').thenReturn(True)
            when(os.path).exists('/dir2').thenReturn(False)
            when(jnsos).is_linux(...).thenReturn(False)
            when(jnsos).is_cygwin(...).thenReturn(False)
            when(jnsvalid).validate_is_directory(...)

        with it('does not raise an error when the rust+git projects are empty'):
            self.opts.rust_git_projects = []
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('does not raise an error when the rust+git projects are None'):
            self.opts.rust_git_projects = None
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('raises an error when not in Linux or Cygwin'):
            expect(lambda: self.updater.validate_opts(self.opts)).to(raise_error)

        with it('does not raise an error when in Linux'):
            when(jnsos).is_linux(...).thenReturn(True)
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('does not raise an error when in Cygwin'):
            when(jnsos).is_cygwin(...).thenReturn(True)
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('validates that each existing project home is a valid directory'):
            when(jnsos).is_linux(...).thenReturn(True)
            self.updater.validate_opts(self.opts)

            verify(jnsvalid).validate_is_directory('/dir1')
            verify(jnsvalid, times(0)).validate_is_directory('/dir2')

    with description(RustGitUpdater.is_root_required):
        with before.each:
            self.md1 = mock({}, spec=RustGitProjectMetaData)

            self.opts = mock({'rust_git_projects': None}, spec=UUUOptions)

        with it('returns true when there are rust+git projects'):
            self.opts.rust_git_projects = ['a', 'b']
            expect(self.updater.is_root_required(self.opts)).to(be_true)

        with it('returns false when the rust+git projects are empty'):
            self.opts.rust_git_projects = []
            expect(self.updater.is_root_required(self.opts)).to(be_false)

        with it('returns false when the rust+git projects are None'):
            self.opts.rust_git_projects = None
            expect(self.updater.is_root_required(self.opts)).to(be_false)

    with description(RustGitUpdater.update):
        with before.each:
            self.opts = mock(UUUOptions)
            self.md1 = mock({'exe_name': 'exe1', 'project_home': '/dir1'}, spec=RustGitProjectMetaData)
            self.md2 = mock({'exe_name': 'exe2', 'project_home': '/dir2'}, spec=RustGitProjectMetaData)
            self.runner = mock(UUURunner)

            when(self.updater)._get_all_project_meta_data(...).thenReturn([self.md1, self.md2])
            when(self.updater)._build(...).thenReturn(['build'])
            when(self.updater)._clone(...).thenReturn(['clone'])
            when(self.updater)._install(...).thenReturn(['install'])
            when(self.updater)._pull(...).thenReturn(['pull'])
            when(os.path).exists('/dir1').thenReturn(True)
            when(os.path).exists('/dir2').thenReturn(False)
            when(self.runner).run(...)

        with it('updates each project when there are projects'):
            self.updater.update(self.opts, self.runner)

            verify(self.runner).run(
                opts=self.opts,
                cmds=[['pull'], ['build'], ['install']],
                title='updating rust+git project: exe1',
            )
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['clone'], ['build'], ['install']],
                title='updating rust+git project: exe2',
            )

        with it('does not update anything when the projects are empty'):
            when(self.updater)._get_all_project_meta_data(...).thenReturn([])
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)

    with description(RustGitUpdater._get_all_project_meta_data):
        with before.each:
            self.opts = mock({'rust_git_projects': ['a', 'b']}, spec=UUUOptions)
            self.md_a = mock(RustGitProjectMetaData)
            self.md_b = mock(RustGitProjectMetaData)

            when(self.updater)._get_project_meta_data('a').thenReturn(self.md_a)
            when(self.updater)._get_project_meta_data('b').thenReturn(self.md_b)

        with it('returns metadata for each project'):
            expect(self.updater._get_all_project_meta_data(self.opts)).to(equal([self.md_a, self.md_b]))

        with it('returns an empty array when the projects are empty'):
            self.opts.rust_git_projects = []
            expect(self.updater._get_all_project_meta_data(self.opts)).to(equal([]))

    with description(RustGitUpdater._get_project_meta_data):
        with it('returns the metadata'):
            md = self.updater._get_project_meta_data('git,home,exe')

            expect(md.git_url).to(equal('git'))
            expect(md.project_home).to(equal('home'))
            expect(md.exe_name).to(equal('exe'))

    with description(RustGitUpdater._clone):
        with it('returns the git clone command'):
            md = mock({'git_url': 'test_git_url', 'project_home': 'test_project_home'}, spec=RustGitProjectMetaData)
            expect(self.updater._clone(md)).to(equal(['git', 'clone', 'test_git_url', 'test_project_home']))

    with description(RustGitUpdater._pull):
        with it('returns the git pull command'):
            md = mock({'project_home': 'test_project_home'}, spec=RustGitProjectMetaData)
            expect(self.updater._pull(md)).to(equal(['git', '-C', 'test_project_home', 'pull']))

    with description(RustGitUpdater._build):
        with it('returns the cargo build command'):
            md = mock({'project_home': '/home/test'}, spec=RustGitProjectMetaData)
            expect(self.updater._build(md)).to(equal([
                'cargo',
                'build',
                '--manifest-path=/home/test/Cargo.toml',
                '--release',
            ]))

    with description(RustGitUpdater._install):
        with before.each:
            when(jnsos).is_linux(...)

        with it('returns the update-alternatives install command using sudo when in Linux'):
            md = mock({'exe_name': 'test_exe', 'project_home': '/home/test'}, spec=RustGitProjectMetaData)
            when(jnsos).is_linux(...).thenReturn(True)

            expect(self.updater._install(md)).to(equal([
                'sudo',
                'update-alternatives',
                '--install',
                '/usr/bin/test_exe',
                'test_exe',
                '/home/test/target/release/test_exe',
                '100',
            ]))

        with it('returns the update-alternatives install command without sudo when not in Linux'):
            md = mock({'exe_name': 'test_exe', 'project_home': '/home/test'}, spec=RustGitProjectMetaData)
            when(jnsos).is_linux(...).thenReturn(False)

            expect(self.updater._install(md)).to(equal([
                'update-alternatives',
                '--install',
                '/usr/bin/test_exe',
                'test_exe',
                '/home/test/target/release/test_exe',
                '100',
            ]))
