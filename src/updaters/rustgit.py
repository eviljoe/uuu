import os.path
from argparse import ArgumentParser
from dataclasses import dataclass

from jnscommons import jnsos, jnsvalid

from updaters.abstractupdater import AbstractUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

_CMD = 'rustgit'
_DELIMITER = ','


@dataclass
class RustGitProjectMetaData:
    git_url: str
    project_home: str
    exe_name: str


class RustGitUpdater(AbstractUpdater):
    def add_help_argument(self, parser: ArgumentParser) -> None:
        parser.add_argument('--' + _CMD, action='append', default=[],
                            metavar='git_url{}project_home{}exe_name'.format(_DELIMITER, _DELIMITER),
                            dest='rust_git_projects',
                            help='Specify a Rust+Git project to be updated.  See below for more information on the '
                                 'argument format.')

    def get_config_command(self) -> str:
        return _CMD

    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        self.update_opts_add_argument(ccmd=ccmd, arguments=opts.rust_git_projects)

    def validate_opts(self, opts: UUUOptions) -> None:
        if opts.rust_git_projects:
            if not (jnsos.is_linux() or jnsos.is_cygwin()):
                raise OSError('The {} updater can only be used in Linux or Cygwin'.format(_CMD))

            for mdata in self._get_all_project_meta_data(opts):
                if os.path.exists(mdata.project_home):
                    jnsvalid.validate_is_directory(mdata.project_home)

    def is_root_required(self, opts: UUUOptions) -> bool:
        return True if opts.rust_git_projects else False

    def update(self, opts: UUUOptions, runner: UUURunner) -> None:
        for mdata in self._get_all_project_meta_data(opts):
            cmds = [
                self._pull(mdata) if os.path.exists(mdata.project_home) else self._clone(mdata),
                self._build(mdata),
                self._install(mdata)
            ]
            runner.run(opts=opts, cmds=cmds, title='updating rust+git project: {}'.format(mdata.exe_name))

    def _get_all_project_meta_data(self, opts: UUUOptions) -> [RustGitProjectMetaData]:
        return [self._get_project_meta_data(project) for project in opts.rust_git_projects]

    @staticmethod
    def _get_project_meta_data(project_string: str) -> RustGitProjectMetaData:
        parts = project_string.split(_DELIMITER)
        return RustGitProjectMetaData(
            git_url=parts[0].strip(),
            project_home=parts[1].strip(),
            exe_name=parts[2].strip()
        )

    @staticmethod
    def _clone(metadata: RustGitProjectMetaData) -> [str]:
        return ['git', 'clone', metadata.git_url, metadata.project_home]

    @staticmethod
    def _pull(metadata: RustGitProjectMetaData) -> [str]:
        return ['git', '-C', metadata.project_home, 'pull']

    @staticmethod
    def _build(metadata: RustGitProjectMetaData) -> [str]:
        return [
            'cargo',
            'build',
            '--manifest-path={}/Cargo.toml'.format(metadata.project_home),
            '--release'
        ]

    @staticmethod
    def _install(metadata: RustGitProjectMetaData) -> [str]:
        cmd = ['sudo'] if jnsos.is_linux() else []
        cmd.extend([
            'update-alternatives',
            '--install',
            '/usr/bin/{}'.format(metadata.exe_name),
            metadata.exe_name,
            '{}/target/release/{}'.format(metadata.project_home, metadata.exe_name),
            '100'
        ])

        return cmd
