from argparse import ArgumentParser

from jnscommons import jnsos

from updaters.abstractupdater import AbstractUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

_CMD = 'npm'


class NPMUpdater(AbstractUpdater):
    def add_help_argument(self, parser: ArgumentParser) -> None:
        parser.add_argument('--' + _CMD, action='append', default=[], metavar='npm_pkg', dest='npm_packages',
                            help='Specify an npm package to be updated')

    def get_config_command(self) -> str:
        return _CMD

    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        self.update_opts_add_argument(ccmd=ccmd, arguments=opts.npm_packages)

    def validate_opts(self, opts: UUUOptions) -> None:
        return

    def is_root_required(self, opts: UUUOptions) -> bool:
        return True if opts.npm_packages else False

    def update(self, opts: UUUOptions, runner: UUURunner) -> None:
        if opts.npm_packages:
            cmd = ['sudo'] if jnsos.is_linux() else []
            cmd.extend(['npm', 'update', '-g'])
            cmd.extend(opts.npm_packages)

            runner.run(opts=opts, cmds=[cmd],
                       title='updating npm packages: {}'.format(', '.join(opts.npm_packages)))
