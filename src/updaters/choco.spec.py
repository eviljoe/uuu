from argparse import ArgumentParser

from expects import be_false, be_true, equal, expect, raise_error
from jnscommons import jnsos
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

from updaters import choco
from updaters.choco import ChocoUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

with description(choco.ChocoUpdater) as self:
    with before.each:
        self.updater = ChocoUpdater()
        self.opts = mock({'choco': None, 'dry_run': None}, spec=UUUOptions)

    with after.each:
        unstub()

    with description(ChocoUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(ChocoUpdater.get_config_command):
        with it('returns "choco"'):
            expect(self.updater.get_config_command()).to(equal('choco'))

    with description(ChocoUpdater.update_opts_for_command):
        with it('sets the choco flag to true'):
            self.updater.update_opts_for_command(self.opts, mock(ConfigCommand))
            expect(self.opts.choco).to(be_true)

    with description(ChocoUpdater.validate_opts):
        with before.each:
            self.opts.choco = True
            self.opts.dry_run = False
            when(jnsos).is_windows(...).thenReturn(False)
            when(jnsos).is_cygwin(...).thenReturn(False)

        with it('raises an error when in an unsupported OS'):
            expect(lambda: self.updater.validate_opts(self.opts)).to(raise_error)

        with it('does not raise an error when the choco flag is false'):
            self.opts.choco = False
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('does not raise an error when performing a dry run'):
            self.opts.dry_run = True
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('does not raise an error when in Windows'):
            when(jnsos).is_windows(...).thenReturn(True)
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

        with it('does not raise an error when in Cygwin'):
            when(jnsos).is_cygwin(...).thenReturn(True)
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

    with description(ChocoUpdater.is_root_required):
        with it('returns false'):
            expect(self.updater.is_root_required(self.opts)).to(be_false)

    with description(ChocoUpdater.update):
        with before.each:
            self.runner = mock(UUURunner)
            when(self.runner).run(...)

        with it('updates when the choco flag is true'):
            self.opts.choco = True
            self.updater.update(self.opts, self.runner)
            verify(self.runner).run(...)

        with it('does not update anything when the choco flag is false'):
            self.opts.choco = False
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)
