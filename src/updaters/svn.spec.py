from argparse import ArgumentParser

from expects import equal, expect
from jnscommons import jnsvalid
from mamba import after, before, description, it
from mockito import mock, unstub, verify, when

from updaters import svn
from updaters.svn import SVNUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions

with description(svn.SVNUpdater) as self:
    with before.each:
        self.updater = SVNUpdater()

    with after.each:
        unstub()

    with description(SVNUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(SVNUpdater.get_config_command):
        with it('returns "svn"'):
            expect(self.updater.get_config_command()).to(equal('svn'))

    with description(SVNUpdater.update_opts_for_command):
        with before.each:
            self.opts = mock(UUUOptions)
            self.ccmd = mock(ConfigCommand)

            when(self.updater).update_opts_add_directory(...)
            when(self.updater).get_repos(...).thenReturn(['a', 'b'])

        with it('adds a directory to the options'):
            self.updater.update_opts_for_command(self.opts, self.ccmd)
            verify(self.updater).update_opts_add_directory(ccmd=self.ccmd, directories=['a', 'b'])

    with description(SVNUpdater.validate_opts):
        with before.each:
            self.opts = mock({'svn_dirs': ['a', 'b']}, spec=UUUOptions)
            when(jnsvalid).validate_is_directories(...)

        with it('validates that each SVN directory is a valid directory'):
            self.updater.validate_opts(self.opts)
            verify(jnsvalid).validate_is_directories(['a', 'b'])

    with description(SVNUpdater.get_config_directory):
        with it('returns ".svn"'):
            expect(self.updater.get_config_directory()).to(equal('.svn'))

    with description(SVNUpdater.get_repo_type):
        with it('returns "svn"'):
            expect(self.updater.get_repo_type()).to(equal('svn'))

    with description(SVNUpdater.get_repos):
        with before.each:
            self.opts = mock({'svn_dirs': ['a', 'b']}, spec=UUUOptions)

        with it('returns the SVN directories'):
            expect(self.updater.get_repos(self.opts)).to(equal(['a', 'b']))

    with description(SVNUpdater.get_update_cmd):
        with it('returns "svn update --non-interactive"'):
            expect(self.updater.get_update_cmd()).to(equal(['svn', 'update', '--non-interactive']))
