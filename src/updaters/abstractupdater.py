import os.path
from abc import ABCMeta
from abc import abstractmethod
from argparse import ArgumentParser

from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner


class AbstractUpdater(metaclass=ABCMeta):

    @abstractmethod
    def add_help_argument(self, parser: ArgumentParser) -> None:
        raise NotImplementedError()

    @abstractmethod
    def get_config_command(self) -> str:
        raise NotImplementedError()

    @abstractmethod
    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        raise NotImplementedError()

    @abstractmethod
    def validate_opts(self, opts: UUUOptions) -> None:
        raise NotImplementedError()

    @abstractmethod
    def is_root_required(self, opts: UUUOptions) -> bool:
        raise NotImplementedError()

    @abstractmethod
    def update(self, opts: UUUOptions, runner: UUURunner) -> None:
        raise NotImplementedError()

    def is_config_command(self, ccmd: ConfigCommand) -> bool:
        return ccmd.cmd.lower() == self.get_config_command().lower()

    @staticmethod
    def update_opts_add_directory(ccmd: ConfigCommand, directories: [str]) -> None:
        _verify_has_argument(ccmd, argtype='directory')
        directories.append(_normalize_home_dir(ccmd.arg))

    @staticmethod
    def update_opts_set_file(ccmd: ConfigCommand, opts: UUUOptions, argument_name: str) -> None:
        _verify_has_argument(ccmd, argtype='file')
        setattr(opts, argument_name, _normalize_home_dir(ccmd.arg))

    @staticmethod
    def update_opts_add_argument(ccmd: ConfigCommand, arguments: [str]) -> None:
        _verify_has_argument(ccmd)
        arguments.append(ccmd.arg)


class AbstractRepoUpdater(AbstractUpdater, metaclass=ABCMeta):

    @abstractmethod
    def get_repos(self, opts: UUUOptions) -> [str]:
        raise NotImplementedError()

    @abstractmethod
    def get_repo_type(self) -> str:
        raise NotImplementedError()

    @abstractmethod
    def get_config_directory(self) -> str:
        raise NotImplementedError()

    @abstractmethod
    def get_update_cmd(self) -> [str]:
        raise NotImplementedError()

    def is_root_required(self, opts: UUUOptions) -> bool:
        return False

    def update(self, opts: UUUOptions, runner: UUURunner) -> None:
        for repo_dir in self.get_repos(opts):
            self.update_repo(opts=opts, runner=runner, directory=repo_dir, repo_type=self.get_repo_type(),
                             config_directory=self.get_config_directory(), cmd=self.get_update_cmd())

    @staticmethod
    def update_repo(opts: UUUOptions, runner: UUURunner,
                    directory: str, repo_type: str, config_directory: str, cmd: [str]) -> None:
        if os.path.exists(os.path.join(directory, config_directory)):
            runner.run(opts=opts, cmds=[cmd], cwd=directory,
                       title='updating {} repository: {}'.format(repo_type, directory))
        else:
            runner.run(opts=opts, title='skipping {} repository with no {} directory: {}'.format(
                repo_type, config_directory, directory))


class AbstractRepoDirUpdater(AbstractRepoUpdater, metaclass=ABCMeta):

    @abstractmethod
    def get_repo_dirs(self, opts: UUUOptions) -> [str]:
        raise NotImplementedError()

    def is_root_required(self, opts: UUUOptions) -> bool:
        return False

    def update(self, opts: UUUOptions, runner: UUURunner) -> None:
        for repo_dir in self.get_repo_dirs(opts):
            for repo in os.listdir(path=repo_dir):
                repo = os.path.join(repo_dir, repo)

                if os.path.isdir(repo):
                    self.update_repo(opts=opts, runner=runner, directory=repo, repo_type=self.get_repo_type(),
                                     config_directory=self.get_config_directory(), cmd=self.get_update_cmd())


# ################# #
# Utility Functions #
# ################# #


def _normalize_home_dir(directory: str) -> str:
    if directory == '~' or directory.startswith('~/') or directory.startswith('~\\'):
        directory = os.path.expanduser('~') + directory[1:]

    return directory


def _verify_has_argument(ccmd: ConfigCommand, argtype: str = 'argument') -> None:
    if not ccmd.arg:
        raise InvalidConfigError('[{}, line {}] Configuration command requires {}: {}'.format(
            ccmd.config_file_name, ccmd.line_num, argtype, ccmd.cmd))


# ########## #
# Exceptions #
# ########## #


class InvalidConfigError(Exception):
    pass
