from argparse import ArgumentParser

from jnscommons import jnsos

from updaters.abstractupdater import AbstractUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

_CMD = 'pip3'


class Pip3Updater(AbstractUpdater):
    def add_help_argument(self, parser: ArgumentParser) -> None:
        parser.add_argument('--' + _CMD, action='append', default=[], metavar='pip3_pkg', dest='pip3_packages',
                            help='Specify a pip3 package to be updated')

    def get_config_command(self) -> str:
        return _CMD

    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        self.update_opts_add_argument(ccmd=ccmd, arguments=opts.pip3_packages)

    def validate_opts(self, opts: UUUOptions) -> None:
        return

    def is_root_required(self, opts: UUUOptions) -> bool:
        return True if opts.pip3_packages else False

    def update(self, opts: UUUOptions, runner: UUURunner) -> None:
        for package in opts.pip3_packages:
            cmd = ['sudo'] if jnsos.is_linux() else []
            cmd.extend(['python3', '-m', 'pip', 'install', '--upgrade', package])

            runner.run(opts=opts, cmds=[cmd],
                       title='updating pip3 package: {}'.format(package))
