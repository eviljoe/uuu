from argparse import ArgumentParser

from jnscommons import jnsvalid

from updaters.abstractupdater import AbstractRepoDirUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions

_CMD = 'svnd'


class SVNDUpdater(AbstractRepoDirUpdater):
    def add_help_argument(self, parser: ArgumentParser) -> None:
        parser.add_argument('--' + _CMD, action='append', default=[], metavar='svn_repo_dir', dest='svnd_dirs',
                            help='Specify a directory that may contain one or more SVN reposotories to be updated')

    def get_config_command(self) -> str:
        return _CMD

    def update_opts_for_command(self, opts: UUUOptions, ccmd: ConfigCommand) -> None:
        self.update_opts_add_directory(ccmd=ccmd, directories=self.get_repo_dirs(opts))

    def validate_opts(self, opts: UUUOptions) -> None:
        jnsvalid.validate_is_directories(opts.svnd_dirs)

    def get_config_directory(self) -> str:
        return '.svn'

    def get_repo_dirs(self, opts: UUUOptions) -> [str]:
        return opts.svnd_dirs

    def get_repos(self, opts: UUUOptions) -> [str]:
        return None

    def get_repo_type(self) -> str:
        return 'svn'

    def get_update_cmd(self) -> [str]:
        return ['svn', 'update', '--non-interactive']
