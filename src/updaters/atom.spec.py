from argparse import ArgumentParser

from expects import be_false, be_true, equal, expect, raise_error
from jnscommons import jnsos
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

from updaters import atom
from updaters.atom import AtomUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

with description(atom.AtomUpdater) as self:
    with before.each:
        self.updater = AtomUpdater()
        self.opts = mock({'atom': None}, spec=UUUOptions)

    with after.each:
        unstub()

    with description(AtomUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(AtomUpdater.get_config_command):
        with it('returns "atom"'):
            expect(self.updater.get_config_command()).to(equal('atom'))

    with description(AtomUpdater.update_opts_for_command):
        with it('sets the atom flag to true'):
            self.updater.update_opts_for_command(self.opts, mock(ConfigCommand))
            expect(self.opts.atom).to(be_true)

    with description(AtomUpdater.validate_opts):
        with it('does not raise an error'):
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

    with description(AtomUpdater.is_root_required):
        with it('returns false'):
            expect(self.updater.is_root_required(self.opts)).to(be_false)

    with description(AtomUpdater.update):
        with before.each:
            self.runner = mock(UUURunner)
            self.opts.atom = True

            when(self.runner).run(...)
            when(jnsos).is_cygwin(...).thenReturn(False)
            when(jnsos).is_windows(...).thenReturn(False)
            when(jnsos).is_linux(...).thenReturn(False)

        with it('updates using "cmd" and "apm" when in Cygwin'):
            when(jnsos).is_cygwin(...).thenReturn(True)
            self.updater.update(self.opts, self.runner)
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['cmd', '/C', 'apm', 'update', '--no-confirm']],
                title="updating atom's packages",
            )

        with it('updates using "apm.cmd" when in Windows'):
            when(jnsos).is_windows(...).thenReturn(True)
            self.updater.update(self.opts, self.runner)
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['apm.cmd', 'update', '--no-confirm']],
                title="updating atom's packages",
            )

        with it('updates using "apm" when in Linux'):
            when(jnsos).is_linux(...).thenReturn(True)
            self.updater.update(self.opts, self.runner)
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['apm', 'update', '--no-confirm']],
                title="updating atom's packages",
            )

        with it('does not update anything when the atom flag is false'):
            self.opts.atom = False
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)
