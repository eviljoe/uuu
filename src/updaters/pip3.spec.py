from argparse import ArgumentParser

from expects import be_false, be_true, equal, expect, raise_error
from jnscommons import jnsos
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

from updaters import pip3
from updaters.pip3 import Pip3Updater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

with description(pip3.Pip3Updater) as self:
    with before.each:
        self.updater = Pip3Updater()
        self.opts = mock({'pip3_packages': None}, spec=UUUOptions)

    with after.each:
        unstub()

    with description(Pip3Updater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(Pip3Updater.get_config_command):
        with it('returns "pip3"'):
            expect(self.updater.get_config_command()).to(equal('pip3'))

    with description(Pip3Updater.update_opts_for_command):
        with before.each:
            self.ccmd = mock(ConfigCommand)
            when(self.updater).update_opts_add_argument(...)

        with it('adds an argument to the options'):
            self.opts.pip3_packages = ['a', 'b']
            self.updater.update_opts_for_command(self.opts, self.ccmd)
            verify(self.updater).update_opts_add_argument(ccmd=self.ccmd, arguments=['a', 'b'])

    with description(Pip3Updater.validate_opts):
        with it('does not raise an error'):
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

    with description(Pip3Updater.is_root_required):
        with it('returns true when there are PIP3 packages'):
            self.opts.pip3_packages = ['a', 'b']
            expect(self.updater.is_root_required(self.opts)).to(be_true)

        with it('returns false when the PIP3 packages are empty'):
            self.opts.pip3_packages = []
            expect(self.updater.is_root_required(self.opts)).to(be_false)

        with it('returns false when the PIP3 packages are None'):
            self.opts.pip3_packages = None
            expect(self.updater.is_root_required(self.opts)).to(be_false)

    with description(Pip3Updater.update):
        with before.each:
            self.runner = mock(UUURunner)
            when(self.runner).run(...)
            when(jnsos).is_linux(...).thenReturn(False)

        with it('updates each pip3 package with sudo when in Linux'):
            self.opts.pip3_packages = ['a', 'b']
            when(jnsos).is_linux(...).thenReturn(True)
            self.updater.update(self.opts, self.runner)

            verify(self.runner).run(
                opts=self.opts,
                cmds=[['sudo', 'python3', '-m', 'pip', 'install', '--upgrade', 'a']],
                title='updating pip3 package: a'
            )
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['sudo', 'python3', '-m', 'pip', 'install', '--upgrade', 'b']],
                title='updating pip3 package: b'
            )

        with it('updates each pip3 package without sudo when not in Linux'):
            self.opts.pip3_packages = ['a', 'b']
            when(jnsos).is_linux(...).thenReturn(False)
            self.updater.update(self.opts, self.runner)

            verify(self.runner).run(
                opts=self.opts,
                cmds=[['python3', '-m', 'pip', 'install', '--upgrade', 'a']],
                title='updating pip3 package: a'
            )
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['python3', '-m', 'pip', 'install', '--upgrade', 'b']],
                title='updating pip3 package: b'
            )

        with it('does not update anything when the pip3 packages are empty'):
            self.opts.pip3_packages = []
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)
