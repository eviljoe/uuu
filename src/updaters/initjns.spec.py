from argparse import ArgumentParser

from expects import expect, equal, be_true, raise_error, be_false
from mamba import after, before, description, it
from mockito import unstub, when, mock, verify, times

from updaters import initjns
from updaters.initjns import InitJNSUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner

with description(initjns.InitJNSUpdater) as self:
    with before.each:
        self.updater = InitJNSUpdater()

    with after.each:
        unstub()

    with description(InitJNSUpdater.add_help_argument):
        with before.each:
            self.parser = mock(ArgumentParser)
            when(self.parser).add_argument(...)

        with it('adds an argument'):
            self.updater.add_help_argument(self.parser)
            verify(self.parser).add_argument(...)

    with description(InitJNSUpdater.get_config_command):
        with it('returns "init-jns"'):
            expect(self.updater.get_config_command()).to(equal('init-jns'))

    with description(InitJNSUpdater.update_opts_for_command):
        with before.each:
            self.opts = mock({'init-jns': None}, spec=UUUOptions)

        with it('sets the init-jns flag to true'):
            self.updater.update_opts_for_command(self.opts, mock(ConfigCommand))
            expect(self.opts.init_jns).to(be_true)

    with description(InitJNSUpdater.validate_opts):
        with before.each:
            self.opts = mock(UUUOptions)

        with it('does not raise an error'):
            expect(lambda: self.updater.validate_opts(self.opts)).not_to(raise_error)

    with description(InitJNSUpdater.is_root_required):
        with before.each:
            self.opts = mock(UUUOptions)

        with it('returns false'):
            expect(self.updater.is_root_required(self.opts)).to(be_false)

    with description(InitJNSUpdater.update):
        with before.each:
            self.opts = mock({'init_jns': None}, spec=UUUOptions)
            self.runner = mock(UUURunner)

            when(self.runner).run(...)

        with it('updates when the init-jns flag is True'):
            self.opts.init_jns = True
            self.updater.update(self.opts, self.runner)
            verify(self.runner).run(
                opts=self.opts,
                cmds=[['init-jns']],
                title='initializing junk-n-stuff',
            )

        with it('does not update when the init-jns flag is False'):
            self.opts.init_jns = False
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)

        with it('does not update when the init-jns flag is None'):
            self.opts.init_jns = None
            self.updater.update(self.opts, self.runner)
            verify(self.runner, times(0)).run(...)
