import os.path

from expects import be_false, be_true, equal, expect, raise_error
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

from updaters import abstractupdater
from updaters.abstractupdater import AbstractRepoDirUpdater, AbstractRepoUpdater, AbstractUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions
from utils.uuurunner import UUURunner


class TestAbstractUpdater(AbstractUpdater):
    def add_help_argument(self, parser):
        pass

    def get_config_command(self):
        pass

    def update_opts_for_command(self, opts, ccmd):
        pass

    def validate_opts(self, opts):
        pass

    def is_root_required(self, opts):
        pass

    def update(self, opts, runner):
        pass


class TestAbstractRepoUpdater(AbstractRepoUpdater):
    def add_help_argument(self, parser):
        pass

    def get_config_command(self):
        pass

    def update_opts_for_command(self, opts, ccmd):
        pass

    def validate_opts(self, opts):
        pass

    def get_repos(self, opts):
        pass

    def get_repo_type(self):
        pass

    def get_config_directory(self):
        pass

    def get_update_cmd(self):
        pass


class TestAbstractRepoDirUpdater(AbstractRepoDirUpdater):

    def add_help_argument(self, parser):
        pass

    def get_config_command(self):
        pass

    def get_config_directory(self):
        pass

    def get_repo_dirs(self, opts):
        pass

    def get_repo_type(self):
        pass

    def get_repos(self, opts):
        pass

    def get_update_cmd(self):
        pass

    def update_opts_for_command(self, opts, ccmd):
        pass

    def validate_opts(self, opts):
        pass


with description(abstractupdater) as self:
    with after.each:
        unstub()

    with description(AbstractUpdater):
        with before.each:
            self.updater = TestAbstractUpdater()

        with description(AbstractUpdater.is_config_command):
            with before.each:
                self.ccmd = mock({'cmd': None}, spec=ConfigCommand)
                when(self.updater).get_config_command().thenReturn('FOO')

            with it("returns true when the command is the same as the updater's command"):
                self.ccmd.cmd = 'FOO'
                expect(self.updater.is_config_command(self.ccmd)).to(be_true)

            with it("returns true when the command is the same as the updater's command (ignoring case)"):
                self.ccmd.cmd = 'Foo'
                expect(self.updater.is_config_command(self.ccmd)).to(be_true)

            with it("returns false when the command is the different form the updater's command"):
                self.ccmd.cmd = 'bar'
                expect(self.updater.is_config_command(self.ccmd)).to(be_false)

        with description(AbstractUpdater.update_opts_add_directory):
            with before.each:
                self.ccmd = mock({'arg': 'foo'}, spec=ConfigCommand)

                when(abstractupdater)._verify_has_argument(...)
                when(abstractupdater)._normalize_home_dir(...).thenReturn('normalized_foo')

            with it('verifies that the command has an argument'):
                self.updater.update_opts_add_directory(self.ccmd, ['a', 'b'])
                verify(abstractupdater)._verify_has_argument(self.ccmd, argtype='directory')

            with it('adds the normalized directory to the given array'):
                dirs = ['a', 'b']
                self.updater.update_opts_add_directory(self.ccmd, dirs)
                expect(dirs).to(equal(['a', 'b', 'normalized_foo']))

        with description(AbstractUpdater.update_opts_set_file):
            with before.each:
                self.ccmd = mock({'arg': 'bar'}, spec=ConfigCommand)
                self.opts = mock({'foo': None}, spec=UUUOptions)

                when(abstractupdater)._verify_has_argument(...)
                when(abstractupdater)._normalize_home_dir(...).thenAnswer(lambda name: 'normalized_' + name)

            with it('verifies that the command has an argument'):
                self.updater.update_opts_set_file(self.ccmd, self.opts, 'foo')
                verify(abstractupdater)._verify_has_argument(self.ccmd, argtype='file')

            with it('sets the normalized file in the options for the given argument name'):
                self.updater.update_opts_set_file(self.ccmd, self.opts, 'foo')
                expect(self.opts.foo).to(equal('normalized_bar'))

        with description(AbstractUpdater.update_opts_add_argument):
            with before.each:
                self.ccmd = mock({'arg': 'foo'}, spec=ConfigCommand)
                when(abstractupdater)._verify_has_argument(...)

            with it('verifies that the command has an argument'):
                self.updater.update_opts_add_argument(self.ccmd, ['a', 'b'])
                verify(abstractupdater)._verify_has_argument(self.ccmd)

            with it('adds the argument to the given array'):
                dirs = ['a', 'b']
                self.updater.update_opts_add_argument(self.ccmd, dirs)
                expect(dirs).to(equal(['a', 'b', 'foo']))

    with description(AbstractRepoUpdater):
        with before.each:
            self.updater = TestAbstractRepoUpdater()

        with description(AbstractRepoUpdater.is_root_required):
            with before.each:
                self.opts = mock(UUUOptions)

            with it('returns false'):
                expect(self.updater.is_root_required(self.opts)).to(be_false)

        with description(AbstractRepoUpdater.update):
            with before.each:
                self.opts = mock(UUUOptions)
                self.runner = mock(UUURunner)

                when(self.updater).get_repos(...).thenReturn(['a', 'b'])
                when(self.updater).get_repo_type(...).thenReturn('test_repo_type')
                when(self.updater).get_config_directory(...).thenReturn('test_config_dir')
                when(self.updater).get_update_cmd(...).thenReturn('test_cmd')
                when(self.updater).update_repo(...)

            with it('updates each repository'):
                self.updater.update(self.opts, self.runner)

                verify(self.updater).update_repo(
                    opts=self.opts,
                    runner=self.runner,
                    directory='a',
                    repo_type='test_repo_type',
                    config_directory='test_config_dir',
                    cmd='test_cmd'
                )
                verify(self.updater).update_repo(
                    opts=self.opts,
                    runner=self.runner,
                    directory='b',
                    repo_type='test_repo_type',
                    config_directory='test_config_dir',
                    cmd='test_cmd'
                )

        with description(AbstractRepoUpdater.update_repo):
            with before.each:
                self.opts = mock(UUUOptions)
                self.runner = mock(UUURunner)

                when(os.path).join('/dir1', '.config').thenReturn('/dir1/.config')
                when(os.path).join('/dir2', '.config').thenReturn('/dir2/.config')
                when(os.path).exists('/dir1/.config').thenReturn(True)
                when(os.path).exists('/dir2/.config').thenReturn(False)
                when(self.runner).run(...)

            with it('runs the update in each directory that has a config directory'):
                self.updater.update_repo(opts=self.opts, runner=self.runner, directory='/dir1', repo_type='test_repo',
                                         config_directory='.config', cmd=['update', 'repo'])

                verify(self.runner).run(
                    opts=self.opts,
                    cmds=[['update', 'repo']],
                    cwd='/dir1',
                    title='updating test_repo repository: /dir1'
                )

            with it('skips directories with no config directories'):
                self.updater.update_repo(opts=self.opts, runner=self.runner, directory='/dir2', repo_type='test_repo',
                                         config_directory='.config', cmd=['update', 'repo'])

                verify(self.runner).run(
                    opts=self.opts,
                    title='skipping test_repo repository with no .config directory: /dir2'
                )

    with description(AbstractRepoDirUpdater):
        with before.each:
            self.updater = TestAbstractRepoDirUpdater()

        with description(AbstractRepoDirUpdater.is_root_required):
            with before.each:
                self.opts = mock(UUUOptions)

            with it('returns false'):
                expect(self.updater.is_root_required(self.opts)).to(be_false)

        with description(AbstractRepoDirUpdater.update):
            with before.each:
                self.opts = mock(UUUOptions)
                self.runner = mock(UUURunner)

                when(self.updater).get_repo_type(...).thenReturn('test_repo')
                when(self.updater).get_config_directory(...).thenReturn('.config')
                when(self.updater).get_repo_dirs(...).thenReturn(['/dirA', '/dirB'])
                when(self.updater).get_update_cmd(...).thenReturn(['update', 'repo'])
                when(self.updater).update_repo(...)
                when(os).listdir(path='/dirA').thenReturn(['a1', 'a2'])
                when(os).listdir(path='/dirB').thenReturn(['b1', 'b2'])
                when(os.path).join('/dirA', 'a1').thenReturn('/dirA/a1')
                when(os.path).join('/dirA', 'a2').thenReturn('/dirA/a2')
                when(os.path).join('/dirB', 'b1').thenReturn('/dirB/b1')
                when(os.path).join('/dirB', 'b2').thenReturn('/dirB/b2')
                when(os.path).isdir(...).thenReturn(True)

            with it('updates each directory in each repository directory'):
                self.updater.update(self.opts, self.runner)
                verify(self.updater).update_repo(
                    opts=self.opts,
                    runner=self.runner,
                    directory='/dirA/a1',
                    repo_type='test_repo',
                    config_directory='.config',
                    cmd=['update', 'repo']
                )
                verify(self.updater).update_repo(
                    opts=self.opts,
                    runner=self.runner,
                    directory='/dirA/a2',
                    repo_type='test_repo',
                    config_directory='.config',
                    cmd=['update', 'repo']
                )
                verify(self.updater).update_repo(
                    opts=self.opts,
                    runner=self.runner,
                    directory='/dirB/b1',
                    repo_type='test_repo',
                    config_directory='.config',
                    cmd=['update', 'repo']
                )
                verify(self.updater).update_repo(
                    opts=self.opts,
                    runner=self.runner,
                    directory='/dirB/b2',
                    repo_type='test_repo',
                    config_directory='.config',
                    cmd=['update', 'repo']
                )

            with it('does not update any repo that is not a valid directory'):
                when(os.path).isdir(...).thenReturn(False)
                verify(self.updater, times(0)).update_repo(...)

    with description(abstractupdater._normalize_home_dir):
        with before.each:
            when(os.path).expanduser(...).thenReturn('/home/test')

        with it('returns the directory with the home dir expanded when the directory is "~"'):
            expect(abstractupdater._normalize_home_dir('~')).to(equal('/home/test'))

        with it('returns the directory with the home dir expanded when the directory starts with "~/"'):
            expect(abstractupdater._normalize_home_dir('~/')).to(equal('/home/test/'))
            expect(abstractupdater._normalize_home_dir('~/foo')).to(equal('/home/test/foo'))

        with it('returns the directory with the home dir expanded when the directory starts with "~\\"'):
            expect(abstractupdater._normalize_home_dir('~\\')).to(equal('/home/test\\'))
            expect(abstractupdater._normalize_home_dir('~\\foo')).to(equal('/home/test\\foo'))

        with it('returns the given directory when the directory starts with "~~"'):
            expect(abstractupdater._normalize_home_dir('~~')).to(equal('~~'))

        with it('returns the given directory when the directory does not start with "~"'):
            expect(abstractupdater._normalize_home_dir('/foo/bar')).to(equal('/foo/bar'))

    with description(abstractupdater._verify_has_argument):
        with before.each:
            self.ccmd = mock({'arg': None}, spec=ConfigCommand)

        with it("raises an error when the command's argument is empty"):
            self.ccmd.arg = ''
            expect(lambda: abstractupdater._verify_has_argument(self.ccmd)).to(raise_error)

        with it("does not raise an error when the command's argument has values"):
            self.ccmd.arg = 'foo'
            expect(lambda: abstractupdater._verify_has_argument(self.ccmd)).not_to(raise_error)
