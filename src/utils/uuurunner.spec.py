import builtins
import subprocess

from expects import equal, expect
from mamba import after, before, description, it
from mockito import arg_that, mock, times, unstub, verify, when

from utils.options import UUUOptions
from utils.uuurunner import UUURunner

with description(UUURunner) as self:
    with after.each:
        unstub()

    with description(UUURunner.run):
        with before.each:
            self.runner = UUURunner()
            self.opts = mock(UUUOptions)

            when(builtins).print(...)
            when(self.runner)._run_cmds(...)

        with it('prints a delimiter when the run count is greater than zero'):
            self.runner.run_count = 1
            self.runner.run(self.opts)
            verify(builtins).print('----------', ...)

        with it('does not print a delimiter when the run count is zero'):
            self.runner.run_count = 0
            self.runner.run(self.opts)
            verify(builtins, times(0)).print('----------', ...)

        with it('prints the title when one is given'):
            self.runner.run(self.opts, title='foo')
            verify(builtins).print('foo', ...)

        with it('does not print the title when the given title is empty'):
            self.runner.run(self.opts, title='')
            verify(builtins, times(0)).print('', ...)

        with it('does not print a title when the given title is None'):
            self.runner.run(self.opts, title=None)
            verify(builtins, times(0)).print(None, ...)

        with it('increments the run count'):
            self.runner.run_count = 5
            self.runner.run(self.opts)
            expect(self.runner.run_count).to(equal(6))

        with it('runs the given commands'):
            self.runner.run(self.opts, [['foo', 'bar'], ['abc', '123']], cwd='/home/foo')
            verify(self.runner)._run_cmds(self.opts, [['foo', 'bar'], ['abc', '123']], '/home/foo')

        with it('prints a status line'):
            self.runner.run(self.opts)
            verify(builtins).print(arg_that(lambda str: str.startswith('finished update in ')), ...)

    with description(UUURunner._run_cmds):
        with before.each:
            self.runner = UUURunner()
            self.opts = mock(UUUOptions)

            when(self.runner)._run_cmd(...).thenReturn(0)

        with it('runs each given command'):
            self.runner._run_cmds(self.opts, [
                ['a1', 'a2'],
                ['b1', 'b2'],
                ['c1', 'c2'],
            ])

            verify(self.runner)._run_cmd(self.opts, ['a1', 'a2'], ...)
            verify(self.runner)._run_cmd(self.opts, ['b1', 'b2'], ...)
            verify(self.runner)._run_cmd(self.opts, ['c1', 'c2'], ...)

        with it('does not run subsequent commands after one returns with a non-zero exit code'):
            when(self.runner)._run_cmd(self.opts, ['a1', 'a2'], ...).thenReturn(0)
            when(self.runner)._run_cmd(self.opts, ['b1', 'b2'], ...).thenReturn(1)
            when(self.runner)._run_cmd(self.opts, ['c1', 'c2'], ...).thenReturn(0)

            self.runner._run_cmds(self.opts, [
                ['a1', 'a2'],
                ['b1', 'b2'],
                ['c1', 'c2'],
            ])

            verify(self.runner)._run_cmd(self.opts, ['a1', 'a2'], ...)
            verify(self.runner)._run_cmd(self.opts, ['b1', 'b2'], ...)
            verify(self.runner, times(0))._run_cmd(self.opts, ['c1', 'c2'], ...)

        with it('returns the exit code of the last run command'):
            when(self.runner)._run_cmd(self.opts, ['a1', 'a2'], ...).thenReturn(0)
            when(self.runner)._run_cmd(self.opts, ['b1', 'b2'], ...).thenReturn(1)
            when(self.runner)._run_cmd(self.opts, ['c1', 'c2'], ...).thenReturn(0)

            expect(self.runner._run_cmds(self.opts, [
                ['a1', 'a2'],
                ['b1', 'b2'],
                ['c1', 'c2'],
            ])).to(equal(1))

    with description(UUURunner._run_cmd):
        with before.each:
            self.runner = UUURunner()
            self.opts = mock({'verbose': None, 'dry_run': None}, spec=UUUOptions)
            self.process = mock(subprocess.Popen)

            when(builtins).print(...)
            when(subprocess).Popen(...).thenReturn(self.process)
            when(self.process).wait(...)
            when(self.process).poll(...).thenReturn(1)

        with it('prints the command when the verbose option is true'):
            self.opts.verbose = True
            self.runner._run_cmd(self.opts, ['foo', 'bar'])
            verify(builtins).print('foo bar', ...)

        with it('does not print the command when the verbose option is false'):
            self.opts.verbose = False
            self.runner._run_cmd(self.opts, ['foo', 'bar'])
            verify(builtins, times(0)).print(...)

        with it('runs the command when the dry run option is false'):
            self.opts.dry_run = False
            self.runner._run_cmd(self.opts, ['foo', 'bar'], cwd='/home/baz')
            verify(subprocess).Popen(['foo', 'bar'], cwd='/home/baz')

        with it('does not run the command when the dry run option is true'):
            self.opts.dry_run = True
            self.runner._run_cmd(self.opts, ['foo'])
            verify(subprocess, times(0)).Popen(...)

        with it('returns the command exit code when the command is run'):
            self.opts.dry_run = False
            expect(self.runner._run_cmd(self.opts, ['foo'])).to(equal(1))

        with it('returns zero when the command is not run'):
            self.opts.dry_run = True
            expect(self.runner._run_cmd(self.opts, ['foo'])).to(equal(0))
