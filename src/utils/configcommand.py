from dataclasses import dataclass


@dataclass
class ConfigCommand:
    cmd: str = None
    arg: str = None
    config_file_name: str = None
    line_num: int = None
