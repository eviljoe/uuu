from dataclasses import dataclass


@dataclass
class UUUOptions:
    atom: bool
    cabal_packages: [str]
    choco: bool
    cygwin_exe: str
    dry_run: bool
    git_dirs: str
    gitd_dirs: str
    init_jns: bool
    no_config: bool
    npm_packages: [str]
    pip3_packages: [str]
    pip_packages: [str]
    rust_git_projects: [str]
    svn_dirs: [str]
    svnd_dirs: [str]
    verbose: bool
