import os
import re

from updaters.abstractupdater import AbstractUpdater
from utils.configcommand import ConfigCommand
from utils.options import UUUOptions


def read_config_file(updaters: [AbstractUpdater], opts: UUUOptions, config_file_name: str) -> None:
    if os.path.isfile(config_file_name):
        _parse_config_file(updaters=updaters, opts=opts, config_file_name=config_file_name)


def _parse_config_file(updaters: [AbstractUpdater], opts: UUUOptions, config_file_name: str) -> None:
    with open(config_file_name, 'r') as config_file:
        line_num = 0

        for line in config_file:
            line_num += 1
            cmd, arg = _parse_config_file_line(line)

            ccmd = ConfigCommand(
                cmd=cmd,
                arg=arg,
                config_file_name=config_file_name,
                line_num=line_num,
            )

            _process_cmd(updaters, opts, ccmd)


def _parse_config_file_line(line: str) -> [str, str]:
    index = 0
    cmd = ''
    arg = ''

    # remove whitespace before command
    while not _at_eol(line, index) and re.match(r'\s', line[index]):
        index += 1

    # create command
    while not _at_eol(line, index) and re.match(r'\S', line[index]):
        cmd += line[index]
        index += 1

    # remove whitespace between command and argument
    while not _at_eol(line, index) and re.match(r'\s', line[index]):
        index += 1

    # create argument
    while not _at_eol(line, index):
        arg += line[index]
        index += 1

    return cmd, arg


# ################## #
# Command Processors #
# ################## #


def _process_cmd(updaters, opts: UUUOptions, ccmd) -> None:
    if _should_process_cmd(ccmd.cmd):
        updater = _get_updater_for_command(updaters, ccmd)

        if updater is None:
            raise InvalidConfigError('[{}, line {}] Invalid Configuration command: {}'.format(
                ccmd.config_file_name, ccmd.line_num, ccmd.cmd))

        updater.update_opts_for_command(opts, ccmd)


def _get_updater_for_command(updaters: [AbstractUpdater], ccmd) -> AbstractUpdater:
    return next(iter([u for u in updaters if u.get_config_command().lower() == ccmd.cmd.lower()]), None)


def _should_process_cmd(cmd: str) -> bool:
    return len(cmd) > 0 and not cmd.startswith('#')


# ################# #
# Utility Functions #
# ################# #


def _at_eol(line: str, index: int) -> bool:
    return len(line) <= index or line[index] == '\n'


def _normalize_home_dir(directory: str) -> str:
    if directory == '~' or directory.startswith('~/') or directory.startswith('~\\'):
        directory = os.path.expanduser('~') + directory[1:]

    return directory


# ########## #
# Exceptions #
# ########## #


class InvalidConfigError(Exception):
    pass
