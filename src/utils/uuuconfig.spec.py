import builtins
import os.path

from expects import be, be_false, be_none, be_true, equal, expect, raise_error
from mamba import after, before, description, it
from mockito import mock, times, unstub, verify, when

from updaters.abstractupdater import AbstractUpdater
from utils import uuuconfig
from utils.configcommand import ConfigCommand
from utils.uuuconfig import InvalidConfigError

with description(uuuconfig) as self:
    with after.each:
        unstub()

    with description(uuuconfig.read_config_file):
        with before.each:
            self.opts = mock({})
            when(os.path).isfile(...)
            when(uuuconfig)._parse_config_file(...)

        with it('parses the config file when it exists'):
            when(os.path).isfile(...).thenReturn(True)
            uuuconfig.read_config_file([], self.opts, 'foo')
            verify(uuuconfig)._parse_config_file(...)

        with it('does not parse the config file when it does not exist'):
            when(os.path).isfile(...).thenReturn(False)
            uuuconfig.read_config_file([], self.opts, 'foo')
            verify(uuuconfig, times(0))._parse_config_file(...)

    with description(uuuconfig._parse_config_file):
        with before.each:
            self.lines = mock({})
            self.file = mock({})
            self.opts = mock({})

            when(self.lines).__iter__(...).thenReturn(iter(['line 1', 'line 2']))
            when(self.file).__enter__(...).thenReturn(self.lines)
            when(self.file).__exit__(...)

            when(builtins).open(...).thenReturn(self.file)
            when(uuuconfig)._parse_config_file_line('line 1').thenReturn(('cmd1', 'arg1'))
            when(uuuconfig)._parse_config_file_line('line 2').thenReturn(('cmd2', 'arg2'))
            when(uuuconfig)._process_cmd(...)

        with it('parses each config file line'):
            uuuconfig._parse_config_file([], self.opts, 'foo')
            verify(uuuconfig)._parse_config_file_line('line 1')
            verify(uuuconfig)._parse_config_file_line('line 2')

        with it('processes each config file line'):
            uuuconfig._parse_config_file([], self.opts, 'foo')

            verify(uuuconfig)._process_cmd([], self.opts,
                                           ConfigCommand(cmd='cmd1', arg='arg1', config_file_name='foo', line_num=1))
            verify(uuuconfig)._process_cmd([], self.opts,
                                           ConfigCommand(cmd='cmd2', arg='arg2', config_file_name='foo', line_num=2))

    with description(uuuconfig._parse_config_file_line):
        with it('returns two empty strings when given an empty line'):
            expect(uuuconfig._parse_config_file_line('')).to(equal(('', '')))

        with it('returns two empty strings when given a line containing only white space'):
            expect(uuuconfig._parse_config_file_line(' \t\t ')).to(equal(('', '')))

        with it('returns the command with an empty arg when given a line with only a command'):
            expect(uuuconfig._parse_config_file_line('foo')).to(equal(('foo', '')))
            expect(uuuconfig._parse_config_file_line(' \t foo')).to(equal(('foo', '')))
            expect(uuuconfig._parse_config_file_line(' \t foo \t ')).to(equal(('foo', '')))

        with it('returns the command and arg when given a line with both'):
            expect(uuuconfig._parse_config_file_line('foo bar')).to(equal(('foo', 'bar')))
            expect(uuuconfig._parse_config_file_line(' \t foo bar')).to(equal(('foo', 'bar')))
            expect(uuuconfig._parse_config_file_line(' \t foo \t bar')).to(equal(('foo', 'bar')))

    with description(uuuconfig._process_cmd):
        with before.each:
            self.opts = mock({})
            self.ccmd = mock({'cmd': 'cmd', 'config_file_name': 'fname', 'line_num': 1}, spec=ConfigCommand)

            self.updater = mock(AbstractUpdater)
            when(self.updater).update_opts_for_command(...)

            when(uuuconfig)._should_process_cmd(...).thenReturn(True)
            when(uuuconfig)._get_updater_for_command(...).thenReturn(self.updater)

        with it('updates the opts if it should be processed'):
            uuuconfig._process_cmd([], self.opts, self.ccmd)
            verify(self.updater).update_opts_for_command(...)

        with it('does not update the opts if it should not be processed'):
            when(uuuconfig)._should_process_cmd(...).thenReturn(False)
            uuuconfig._process_cmd([], self.opts, self.ccmd)
            verify(self.updater, times(0)).update_opts_for_command(...)

        with it('raises an exception when no updater can be found for the command'):
            when(uuuconfig)._get_updater_for_command(...).thenReturn(None)
            expect(lambda: uuuconfig._process_cmd([], self.opts, self.ccmd)).to(raise_error(InvalidConfigError))

    with description(uuuconfig._get_updater_for_command):
        with before.each:
            self.updater_a = mock(AbstractUpdater)
            self.updater_b = mock(AbstractUpdater)
            self.updater_c = mock(AbstractUpdater)
            self.ccmd = mock(ConfigCommand)

            when(self.updater_a).get_config_command(...).thenReturn('Aa')
            when(self.updater_b).get_config_command(...).thenReturn('Bb')
            when(self.updater_c).get_config_command(...).thenReturn('Cc')
            self.updaters = [self.updater_a, self.updater_b, self.updater_c]

        with it('returns the updater for the given command (ignoring case)'):
            self.ccmd.cmd = 'bb'
            expect(uuuconfig._get_updater_for_command(self.updaters, self.ccmd)).to(be(self.updater_b))

        with it('returns None when there is no updater for the given command'):
            self.ccmd.cmd = 'zz'
            expect(uuuconfig._get_updater_for_command(self.updaters, self.ccmd)).to(be_none)

    with description(uuuconfig._should_process_cmd):
        with it("returns true when the command's length is greater than zero and the command does not start with #"):
            expect(uuuconfig._should_process_cmd('foo')).to(be_true)

        with it('returns false when the command is empty'):
            expect(uuuconfig._should_process_cmd('')).to(be_false)

        with it('returns false when the command starts with #'):
            expect(uuuconfig._should_process_cmd('#foo')).to(be_false)

    with description(uuuconfig._at_eol):
        with it('returns true when the given index is greater than the line length'):
            expect(uuuconfig._at_eol('12345', 6)).to(be_true)

        with it('returns true when the given index is equal to the line length'):
            expect(uuuconfig._at_eol('12345', 5)).to(be_true)

        with it('returns true when character at the given index is a newline'):
            expect(uuuconfig._at_eol('12\n45', 2)).to(be_true)

        with it('returns false when the given index is less than the line length'):
            expect(uuuconfig._at_eol('12345', 4)).to(be_false)

    with description(uuuconfig._normalize_home_dir):
        with before.each:
            when(os.path).expanduser(...).thenReturn('/home/test')

        with it('returns the directory with the home dir expanded when the directory is "~"'):
            expect(uuuconfig._normalize_home_dir('~')).to(equal('/home/test'))

        with it('returns the directory with the home dir expanded when the directory starts with "~/"'):
            expect(uuuconfig._normalize_home_dir('~/')).to(equal('/home/test/'))
            expect(uuuconfig._normalize_home_dir('~/foo')).to(equal('/home/test/foo'))

        with it('returns the directory with the home dir expanded when the directory starts with "~\\"'):
            expect(uuuconfig._normalize_home_dir('~\\')).to(equal('/home/test\\'))
            expect(uuuconfig._normalize_home_dir('~\\foo')).to(equal('/home/test\\foo'))

        with it('returns the given directory when the directory starts with "~~"'):
            expect(uuuconfig._normalize_home_dir('~~')).to(equal('~~'))

        with it('returns the given directory when the directory does not start with "~"'):
            expect(uuuconfig._normalize_home_dir('/foo/bar')).to(equal('/foo/bar'))
