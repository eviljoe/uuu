#!/bin/bash
# Lint the shell scripts

set -euo pipefail
shopt -s globstar

this_script="$(readlink -f "${BASH_SOURCE[0]}")"
this_script_dir="$(dirname "${this_script}")"
cd "${this_script_dir}/.."

printf "* ********** *\n"
printf "* shellcheck *\n"
printf "* ********** *\n"

shellcheck -- **/*.bash
