#!/bin/bash
# Run `uuu.py'

set -euo pipefail

this_script="$(readlink -f "${BASH_SOURCE[0]}")"
this_script_dir="$(dirname "${this_script}")"
cd "${this_script_dir}/../src"

pipenv run ./uuu.py "$@"
