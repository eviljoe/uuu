#!/bin/bash
# Run all of the linters

set -euo pipefail

this_script="$(readlink -f "${BASH_SOURCE[0]}")"
this_script_dir="$(dirname "${this_script}")"
cd "${this_script_dir}/.."

scripts/lint-shell.bash
echo
scripts/lint-python.bash
