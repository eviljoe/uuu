#!/bin/bash
# Run the Python unit tests

set -euo pipefail
shopt -s globstar

this_script="$(readlink -f "${BASH_SOURCE[0]}")"
this_script_dir="$(dirname "${this_script}")"
cd "${this_script_dir}/.."

printf "* ***** *\n"
printf "* mamba *\n"
printf "* ***** *\n"

cd src
mkdir --parents ../coverage
pipenv run mamba --enable-coverage --coverage-file=../coverage/mamba-coverage -- **/*.spec.py
