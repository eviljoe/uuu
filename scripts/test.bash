#!/bin/bash
# Run all of the unit tests

set -euo pipefail

this_script="$(readlink -f "${BASH_SOURCE[0]}")"
this_script_dir="$(dirname "${this_script}")"
cd "${this_script_dir}/.."

scripts/test-python.bash
