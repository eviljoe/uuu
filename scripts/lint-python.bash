#!/bin/bash
# Lint the python files

set -euo pipefail
shopt -s globstar

this_script="$(readlink -f "${BASH_SOURCE[0]}")"
this_script_dir="$(dirname "${this_script}")"
cd "${this_script_dir}/.."

printf "* ****** *\n"
printf "* pylint *\n"
printf "* ****** *\n"

pipenv run pylint \
    --rcfile=.pylintrc \
    --ignore-patterns='.*\.spec\.py' \
    --disable=similarities \
    --output-format=colorized \
    -- **/*.py

printf "* ************ *\n"
printf "* pylint specs *\n"
printf "* ************ *\n"

pipenv run pylint \
    --rcfile=.pylintrc.spec \
    --ignore-patterns='[^.]*\.(?!spec\.)py' \
    --disable=similarities \
    --output-format=colorized \
    -- **/*.py